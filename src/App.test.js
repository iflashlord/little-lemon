import { render, screen } from "@testing-library/react";

import { Reservations } from "./components";

test("Checking Time Status Section", () => {
  render(<Reservations />);

  const heading = screen.getByText("Evening");

  expect(heading).toBeInTheDocument();
});
