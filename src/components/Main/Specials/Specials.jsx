import React from 'react';
import { AppWrap } from '../../../wrapper';
import './Specials.css';
import { MdDeliveryDining } from 'react-icons/md';
import styled, { keyframes } from 'styled-components';

const foodItems = [
  {
    name: 'Greek Salad',
    price: '$22.99',
    description: 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ab tempora deleniti eum repellat explicabo ratione quisquam fugiat quidem.',
    image: "https://loremflickr.com/320/220/salad",
  },
  {
    name: 'Bruschetta',
    price: '$23.99',
    description: 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ab tempora deleniti eum repellat explicabo ratione quisquam fugiat quidem.',
    image: "https://loremflickr.com/320/220/bruschetta",
  },
  {
    name: 'Lemon Dessert',
    price: '$35.00',
    description: 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ab tempora deleniti eum repellat explicabo ratione quisquam fugiat quidem.',
    image: "https://loremflickr.com/320/220/dessert",
  },
  {
    name: 'New Recipe',
    price: '$45.00',
    description: 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ab tempora deleniti eum repellat explicabo ratione quisquam fugiat quidem.',
    image: "https://loremflickr.com/320/220/food",
  },
];
 
  
const slideInUpAnimation = keyframes`
  0% {
    transform: translateY(100%);
    opacity: 0;
  }
  100% {
    transform: translateY(0);
    opacity: 1;
  }
`;

const FoodCardWrapper = styled.div`
  animation: ${slideInUpAnimation} 0.5s ease-in-out;
`;

const FoodCard = ({ props }) => {
  return (
    <FoodCardWrapper>
      <div className="app__specials-item">
        <div className="app__specials-image"   >
            <img src={props.image} alt={props.name}/>
        </div>
        <div className="app_specials-details">
          <div className="app__specials-name-price">
            <h3 className="app_specials-name">{props.name}</h3>
            <p className="app_specials-price">{props.price}</p>
          </div>
          <p className="app_specials-description">{props.description}</p>
          <button className="app__specials-order">
            Order
            <MdDeliveryDining className="app_specials-delivery" />
          </button>
        </div>
      </div>
    </FoodCardWrapper>
  );
};

const Specials = () => {
  return (
    <div className="app__specials">
      <div className="app__specials-title-and-btn">
        <h1 className="app__specials-title">Specials</h1>
        <button className="app__specials-btn-menu">
          Online Menu
        </button>
      </div>
      <div className="app__specials-food-card-holder">
        <div className="app__specials-food">
          {foodItems.map((item, index) => (
            <FoodCard key={index} props={item} />
          ))}
        </div>
      </div>
    </div>
  );
};

export default AppWrap(Specials, 'Menu', 'app__specials');
