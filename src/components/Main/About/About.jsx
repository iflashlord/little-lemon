import React from 'react';
import './About.css';
import { AppWrap } from '../../../wrapper';
 
const aboutData = {
  title: 'Little Lemon',
  subTitle: 'Manchester',
  description: `Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ab tempora deleniti eum repellat explicabo ratione quisquam fugiat quidem.
   Tempora eius deserunt corrupti esse nemo earum, a doloremque minus molestias! Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ab tempora deleniti eum repellat explicabo ratione quisquam fugiat quidem.
   Tempora eius deserunt corrupti esse nemo earum, a doloremque minus molestias!  `,
  image1: "https://loremflickr.com/300/400/restaurant",
  image2: "https://loremflickr.com/300/400/food",
};

const About = () => {
  return (
    <div className="app_about-section">
      <div className="app_about-description-box">
        <h1 className="app__about-title">{aboutData.title}</h1>
        <br />
        <h4 className="app__about-subtitle">{aboutData.subTitle}</h4>
        <br />
        <p className="app__about-description">
          {aboutData.description}
          <br />
          <br />
          {aboutData.description}
        </p>
      </div>

      <div className="app__about-image-holder">
        <div
          className="app__about-image-box img-box-1"
          style={{
            backgroundImage: `url(${aboutData.image2})`
          }}
        />
        <div
          className="app__about-image-box img-box-2"
          style={{
            backgroundImage: `url(${aboutData.image1})`
          }}
        />
      </div>
    </div>
  );
};

export default AppWrap(About, 'About', 'app__about');
