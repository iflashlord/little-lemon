import React from "react";
import './Navbar.css';
 import {HiMenuAlt4, HiX} from 'react-icons/hi';
import { useState } from "react"; 
import {Link } from "react-router-dom"; 
import { HashLink } from 'react-router-hash-link';
import logo from "../../assets/Logo.svg";

const Navbar = () => {
    const [toggle, setToggle] = useState(false);

    return (
    <nav className="app__navbar"> 
            <div className="app__navbar-logo">
                <img src={logo} alt="logo" />
            </div>

        <ul className="app__navbar-links">
            <Link className="link" to={"/#"}>Home</Link> 
            <HashLink className="link" smooth to="/#Menu">Menu</HashLink>
            <HashLink className="link" smooth to="/#Testimonials">Testimonials</HashLink>
            <HashLink className="link" smooth to="/#About">About</HashLink>
        </ul>
            
        <div className="app__navbar-menu">
            <HiMenuAlt4
                onClick={() => { setToggle(true) }}
                className="app__navbar-hamburger"
            />
            {
             toggle &&  (
             <div className="app__navbar-sidebar">
                <HiX 
                    onClick={() => {
                        setToggle(false)
                    }} 
                    className="app__navbar-cancel"
                />

                <Link className="link" to={"/"} onClick={() => { setToggle(false)}}>Home</Link>
                <HashLink className="link" smooth to="/#Menu" onClick={() => { setToggle(false)}}>Menu</HashLink>
                <HashLink className="link" smooth to="/#Testimonials" onClick={() => { setToggle(false)}}>Testimonials</HashLink>
                <HashLink className="link" smooth to="/#About" onClick={() => { setToggle(false)}}>About</HashLink>     

             </div>)
            }
        </div>
    </nav>
    );
}


export default Navbar;