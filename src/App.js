import "./App.css";
import { useState, useEffect, React } from "react";
import { Navbar, Header, Main, Footer } from "./components";
import { Routes, Route, useLocation } from "react-router-dom";
import { headerData } from "./constants";
import DataContext from "./DataContext";

function App() {
  const location = useLocation();
  const [path, setPath] = useState(location.pathname);

  useEffect(() => {
    setPath(location.pathname);
  }, [location.pathname]);

  let data = {};
  if (path === "/") {
    data = headerData;
  }

  return (
    <>
      <Navbar />
      <DataContext.Provider value={data}>
        <Routes>
          <Route
            path="/"
            element={
              <>
                <Header />
                <Main />
              </>
            }
          />
        </Routes>
      </DataContext.Provider>
      <Footer />
    </>
  );
}

export default App;
