const headerData = {
  title: "Little Lemon",
  location: "Manchester",
  description:
    "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ab tempora deleniti eum repellat explicabo ratione quisquam fugiat quidem. Tempora eius deserunt corrupti esse nemo earum, a doloremque minus molestias! Quia.",
  buttonName: "Reserve a Table",
  image: "https://loremflickr.com/450/500/restaurant",
};

export default headerData;
